package dk.groenbaek.application.login;

import dk.groenbaek.application.dto.ApiAuthentication;
import dk.groenbaek.application.dto.Status;
import dk.groenbaek.application.dto.UserInfo;
import dk.groenbaek.application.jpa.entities.RegisteredUser;
import dk.groenbaek.application.jpa.entities.Token;
import dk.groenbaek.application.jpa.entities.UserEvent;
import dk.groenbaek.application.jpa.repositories.TokenRepository;
import dk.groenbaek.application.jpa.repositories.UserEventRepository;
import dk.groenbaek.application.jpa.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * This is a fixed version of the user service.
 * Instead of adding the token and event to the collection and then saving the user again, the token and event are stored
 * using the respective repositories. This is because adding to a JPA collection, causes EclipseLink's IndirectList to
 * lazy load all previous events and tokens, add the new entity, and then when the user is saved it iterates the
 * collection to find and persists the new entity
 *
 * @author Klaus Groenbaek
 *
 */
@Component
public class FixedUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserEventRepository eventRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Transactional
    public ApiAuthentication authenticate(String userName, String password) {
        RegisteredUser user = userRepository.findUser(userName);
        if (user != null) {
            if (user.validPassword(password)) {
                Token token = new Token().setToken(UUID.randomUUID().toString()).setUser(user);
                UserEvent userEvent = new UserEvent().setEventType(UserEvent.EventType.Login).setUser(user);
                tokenRepository.save(token);
                eventRepository.save(userEvent);
                return new ApiAuthentication().setToken(token.getToken());
            }
        }
        return new ApiAuthentication().setError("Invalid username or password");
    }

    @Transactional
    public Status signup(String userName, String password) {
        RegisteredUser user = userRepository.findUser(userName);
        if (user == null) {
            user = RegisteredUser.create(userName, password);
            user.getEvents().add(new UserEvent().setEventType(UserEvent.EventType.Signup).setUser(user));
            userRepository.save(user);
            return new Status();
        } else {
            return new Status().setError("User already exist");
        }
    }

    public UserInfo userInfo(String tokenValue) {
            if (tokenValue == null) {
                return new UserInfo().setError("You need to be authorized to access this endpoint.");
            }
            Token token = tokenRepository.findToken(tokenValue);
            if (token == null) {
                return new UserInfo().setError("Invalid authentication token");
            }
            return new UserInfo()
                    .setUserName(token.getUser().getUsername())
                    .setEventCount(eventRepository.eventPerUser(token.getUser()));
        }
}
