package dk.groenbaek.application.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
@Accessors(chain = true)
public class UserInfo {
    private String error;
    private String userName;
    private int eventCount;

}
