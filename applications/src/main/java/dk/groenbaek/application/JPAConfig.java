package dk.groenbaek.application;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

import static org.eclipse.persistence.config.PersistenceUnitProperties.*;

/**
 * @author Klaus Groenbaek
 *
 */
@Configuration
@EnableTransactionManagement()
@Slf4j
public class JPAConfig {

    @Autowired
    private DataSourceProvider dataSourceProvider;

    private JpaVendorAdapter jpaVendorAdapter() {
        EclipseLinkJpaVendorAdapter jpaVendorAdapter = new EclipseLinkJpaVendorAdapter();
        jpaVendorAdapter.setDatabase(dataSourceProvider.getDatabase());
        return jpaVendorAdapter;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("eclipselink.logging.parameters", "true");
        jpaProperties.setProperty(WEAVING, "false");
        jpaProperties.setProperty(CACHE_SHARED_DEFAULT, "false");
        jpaProperties.put(DDL_GENERATION, PersistenceUnitProperties.CREATE_ONLY);
        jpaProperties.put(DDL_GENERATION_MODE, DDL_DATABASE_GENERATION);


//        jpaProperties.put(LOGGING_FILE, "output.log");
//        jpaProperties.put(LOGGING_LEVEL, "FINE");

        LocalContainerEntityManagerFactoryBean lemfb = new LocalContainerEntityManagerFactoryBean();
        lemfb.setDataSource(dataSource);
        lemfb.setJpaVendorAdapter(jpaVendorAdapter());
        lemfb.setJpaProperties(jpaProperties);
        lemfb.setPackagesToScan("dk.groenbaek.application.jpa");
        return lemfb;
    }

    @Bean
    public DataSource derbyDataSource() {
        return dataSourceProvider.getDataSource();
    }

}