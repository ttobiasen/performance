package dk.groenbaek.application.jpa.repositories;

import dk.groenbaek.application.jpa.entities.RegisteredUser;
import dk.groenbaek.application.jpa.entities.UserEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Klaus Groenbaek
 *
 */
@Repository
public interface UserEventRepository extends JpaRepository<UserEvent, Long> {

    @Query("select count(e) from UserEvent e where e.user =:user")
    int eventPerUser(@Param("user") RegisteredUser user);
}
