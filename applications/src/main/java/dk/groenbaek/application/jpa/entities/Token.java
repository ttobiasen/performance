package dk.groenbaek.application.jpa.entities;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author Klaus Groenbaek
 *
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;

    @ManyToOne
    private RegisteredUser user;

    @Column(unique = true)
    private String token;

}
