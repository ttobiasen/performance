package dk.groenbaek.application.jpa;

import java.util.ArrayList;
import java.util.Collection;

/**
 * JPA helper methods
 * @author Klaus Groenbaek
 *
 */
public class JPAUtils {
    private JPAUtils() {
    }

    /**
     * will return a new collection of the given collection is null.
     * This is used to avoid NullPointerExceptions because JPA collections null when it is not loaded from the database.
     * And we don't the client code to check and set the collection at every location where a new entity could be created
     */
    public static <T> Collection<T> assignCollection(Collection<T> field) {
        if(field == null) {
            field = new ArrayList<>();
        }
        return field;
    }
}
