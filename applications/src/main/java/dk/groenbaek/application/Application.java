package dk.groenbaek.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;

/**
 * @author Klaus Groenbaek
 *
 */
@Slf4j
public class Application {

    public static void main(String[] args) throws Exception {

        SpringApplication.run(WebConfig.class, args);
        log.info("Application started.");

    }

}
