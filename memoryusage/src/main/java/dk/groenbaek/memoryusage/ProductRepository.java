package dk.groenbaek.memoryusage;

import dk.groenbaek.memoryusage.dto.Product;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author Klaus Groenbaek
 *
 */
@Component
public class ProductRepository {

    @Getter
    private Map<Integer, Product> items = new HashMap<>();

    public ProductRepository() {
        List<String> descriptions = WordUtil.randomStrings(200, 4);
        int count = 0;
        Random rnd = new Random();
        for (String description : descriptions) {
            double price = rnd.nextInt(5000) / 100.0 + 10;
            int id = count++;
            items.put(id, new Product().setId(id).setDescription(id + " " + description).setPricePerItem(price));
        }
    }

    public Collection<Product> getProducts() {
        return items.values();
    }

    public Product getItem(int id) {
        return items.get(id);
    }
}
