package dk.groenbaek.cpuusage;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Very simple server with a single endpoint /data, that returns the same string
 * @author Klaus Groenbaek
 *
 */
@SpringBootApplication
@Controller
public class Server {

    @RequestMapping(value = "/data")
    @ResponseBody
    public String data() {
        return "OK";
    }

    public static void main(String[] args) throws Exception {
        new SpringApplicationBuilder(Server.class).run(args);
    }
}
