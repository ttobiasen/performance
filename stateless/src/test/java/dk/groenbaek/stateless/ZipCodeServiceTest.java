package dk.groenbaek.stateless;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Klaus Groenbaek
 *
 */
public class ZipCodeServiceTest {

    @Test
    public void testAll() {
        ZipCodeService service = new ZipCodeService();
        List<ZipCode> all = service.getAll();
        assertEquals("zip code count", 43582, all.size());
    }

    @Test
    public void testSections() {

         for (int i=0; i <=9; i++) {
             ZipCodeService service = new ZipCodeService();
             service.getSection(Character.forDigit(i, 10));
         }
    }
}
